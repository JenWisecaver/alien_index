#!/bin/sh
export CWD=$PWD


### USER SPECIFIED VARIABLES ###
QUERY="$CWD/example/Ecuniculi_pep.fa"			# path to query sequences in FASTA format		
JOBS=100										# number of jobs to split the query into

# PBS variables
USER='jen.wisecaver@vanderbilt.edu'		# user email
HNCPUS='select=1:ncpus=8:mem=15gb'		# number of cpus requested for high cpu/mem jobs
HCPUT='cput=768:0:0' 					# amount of resources that will be encumbered for high cpu/mem jobs where cput=ncpus*walltime
NCPUS='select=1:ncpus=1:mem=2gb'		# number of cpus requested for low cpu/mem jobs
CPUT='cput=96:0:0' 					# amount of resources that will be encumbered for low cpu/mem jobs where cput=ncpus*walltime
WT='walltime=96:0:0'					# maximum physical walltime 

# BLAST parameters
PROGRAM="blastp"             		# what flavor of blast do you want to run?
EVAL='0.99'                  		# evalue threshold
DB="nr" 							# path to database (eg nr, refseq, or custom) to blast against
CPU=8								# number of CPUs to use per job, should match whatever is specified in $NCPUS 
MAXSEQS=1000						# maximum number of blast hits to include in output

# Alien-index parameters
GROUP='4751'
RECIPIENT='6029'

### Directory information ###
SPLITDIR="$CWD/split"
SCRIPTS="$CWD/scripts"
PBSDIR="$CWD/pbsout"
BOUTDIR="$CWD/blast-out"
AOUTDIR="$CWD/alien-out"

echo "Creating working directories"
if [ ! -d "$SPLITDIR" ]; then mkdir $SPLITDIR; fi
if [ ! -d "$BOUTDIR" ]; then mkdir  $BOUTDIR; fi
if [ ! -d "$PBSDIR" ]; then mkdir  $PBSDIR; fi
if [ ! -d "$AOUTDIR" ]; then mkdir  $AOUTDIR; fi

## Preprocess fasta file
echo "Creating job array input files"
perl $SCRIPTS/create_jobarray_fasta.pl $QUERY $SPLITDIR $JOBS

export QUERY PROGRAM EVAL DB CPU MAXSEQS BOUTDIR AOUTDIR GROUP RECIPIENT

##  1 - Submit blast job
echo "Submitting blast job array -N $NAME -J 1-$JOBS $SCRIPTS/run_blast.sh"
FIRST=`qsub -V -N blast -M $USER -l $HNCPUS -l $HCPUT -l $WT -o $PBSDIR -e $PBSDIR -t 1-$JOBS $SCRIPTS/run_blast.sh`

##  2 - Parse blast outputs 
echo "Submitting parsing job array -N alien -t 1-$JOBS $SCRIPTS/run_alienindex.sh"
SECOND=`qsub -V -W depend=afterokarray:$FIRST -N alien -M $USER -l $NCPUS -l $CPUT -l $WT -o $PBSDIR -e $PBSDIR -t 1-$JOBS $SCRIPTS/run_alienindex.sh`

THIRD=`qsub -V -W depend=afterokarray:$SECOND -N cat -M $USER -l $NCPUS -l $CPUT -l $WT -o $PBSDIR -e $PBSDIR $SCRIPTS/run_cat.sh`

echo "All jobs submitted."

