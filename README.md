# Identify potential HGT using the Alien Index

As described by Gladyshev, Eugene A., Matthew Meselson, and Irina R. Arkhipova. "Massive horizontal gene transfer in bdelloid rotifers." science 320.5880 (2008): 1210-1213.

First divide and parallelize a large BLAST job using a PBS job array
Number of jobs is user specified and dependent on the resources available. 

The **example/** directory contains E. cuniculi predicted proteins. Default values will blast Ecuniculi_pep.fa against NCBI's nr database using a one hundred job array (JOBS=100). 

###Download NCBI nr and taxonomy databases
Use NCBI's update_blastdb.pl (http://www.ncbi.nlm.nih.gov/blast/docs/update_blastdb.pl) 

see also: http://www.ncbi.nlm.nih.gov/books/NBK1763/

```
update_blastdb.pl nr -decompress
update_blastdb.pl taxdb -decompress
```

Add the path to these databases to your $BLASTDB environmental variable by adding it to the end of your .cshrc file (for csh and tcsh), .profile file (for sh and ksh), or .bash_profile file (for bash).

```
export BLASTDB="/path/to/your/ncbi/databases:$BLASTDB"
```

###USAGE 

Create a working copy of the blastpipe directory 

```
cp -r alien_index/ analysis/alien_index/.
```

Modify the following lines in the AlienIndex.sh file as necessary
```
#!bash
QUERY="$CWD/example/Ecuniculi_pep.fa"			# path to query sequences in FASTA format		
JOBS=100									# number of jobs to split the query into

# PBS variables
USER='jen.wisecaver@vanderbilt.edu'		# user email
HNCPUS='select=1:ncpus=8:mem=15gb'	# number of cpus requested for high cpu/mem jobs
HCPUT='cput=768:0:0' 					# amount of resources that will be encumbered for high cpu/mem jobs where cput=ncpus*walltime
NCPUS='select=1:ncpus=1:mem=2gb'		# number of cpus requested for low cpu/mem jobs
CPUT='cput=96:0:0' 					# amount of resources that will be encumbered for low cpu/mem jobs where cput=ncpus*walltime
WT='walltime=96:0:0'					# maximum physical walltime 

# BLAST parameters
PROGRAM="blastp"             		# what flavor of blast do you want to run?
EVAL='0.99'                  		        # evalue threshold
DB="nr" 				                # path to database (eg nr, refseq, or custom) to blast against
CPU=8						# number of CPUs to use per job, should match whatever is specified in $NCPUS 
MAXSEQS=1000				# maximum number of blast hits to include in output

# Alien-index parameters
GROUP='4751'                   # ncbi taxonomy id of clade that vertically inherited query sequences should group within
RECIPIENT='6029'                        # ncbi taxonomy id of clade which encompases the query sequences
```

Execute AlienIndex.sh

```
./AlienIndex.sh
```

### OUTPUT

The **split/** directory contains the query file divided into as many files as was specified using the JOBS variable in BlastPipeline.sh

The **blast-out/** directory contains the blast output, also divided.

The **alien-out/** directory contains the calculated alien index output, also divided.

The **combinedresults_alienindex.txt** contains the combined alien index outputs with column descriptions.

Finally the **pbsout/** directory contains the PBS output and error files.