#!/usr/local/bin/perl
use lib "/home/wisecajh/bin/lib/perl5/";
use strict;
use warnings;
use Bio::DB::Taxonomy;
use Bio::Tree::Tree;


my $taxonid = $ARGV[0];
my $lineages = $ARGV[1];

my %lin;
open (IFIL, '<', $lineages) or die "Couldn't open file $lineages: $!\n";
while ( my $line = <IFIL> ) {
	chomp $line;
	my @col = split(/\t/, $line);
	$lin{$col[1]} = $col[0];
}
close IFIL;


my $db = Bio::DB::Taxonomy->new(-source => 'entrez');

my $taxon = $db->get_taxon(-taxonid => $taxonid);
my $cname = $taxon->scientific_name();
my $tree = Bio::Tree::Tree->new(-node => $taxon);
my @taxa = $tree->get_nodes;
my @tids = ();
my @rev_tids = reverse @taxa;
foreach my $t (@rev_tids) {
    my $tname = $t->id();
	#print "$tname\n";
	if (exists $lin{$tname}){
		print "$lin{$tname}\t$cname";
		last;
	}
}
