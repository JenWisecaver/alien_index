#!/bin/bash
### Request email when job begins and ends
#PBS -m bea

echo $PBS_JOBID
echo $BLASTDB

PBS_JOBARRAY=${PBS_JOBID#*\[}
PBS_JOBARRAY=${PBS_JOBARRAY%\]*}

PROGRAM="perl $CWD/scripts/calculate_alien_index.pl"

IN="$CWD/blast-out"
OUT="$CWD/alien-out"

BLASTFIL="$IN/blastout.$PBS_JOBARRAY"

echo parsing $BLASTFIL

cd $OUT

time $PROGRAM -b $BLASTFIL -d $DB -g $GROUP -k $RECIPIENT 

echo
echo
qstat -f $PBS_JOBID
