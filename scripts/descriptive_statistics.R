# usage: Rscript descriptive_statistics.R inputfile

# load required libraries
.libPaths("/home/wisecajh/R/rlib")
library(Hmisc)
args <- commandArgs(trailingOnly = TRUE)
inputfile <- args[1]

ai <- scan(inputfile, sep="\n")

quartz(width = 6, height = 6, type = "pdf", file = "ai_histogram.pdf")
h <- hist(ai)
h

quartz(width = 6, height = 6, type = "pdf", file = "ai_plot.pdf")
p <- plot(ai)
p

stats<- describe(ai)
print (stats)