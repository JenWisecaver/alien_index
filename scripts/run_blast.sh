#!/bin/sh
### Request email when job begins and ends
#PBS -m bea

echo $PBS_JOBID
echo $BLASTDB

PBS_JOBARRAY=${PBS_JOBID#*\[}
PBS_JOBARRAY=${PBS_JOBARRAY%\]*}

# for blast
QUERY="$CWD/split/query.$PBS_JOBARRAY" 
BOUTPUT="$BOUTDIR/blastout.$PBS_JOBARRAY"

cd $CWD

echo submitting: $PROGRAM -query $QUERY -out $BOUTPUT -db $DB -evalue $EVAL -num_threads $CPU -max_target_seqs $MAXSEQS -outfmt '6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore saccver slen qlen nident positive staxids qcovs'

time $PROGRAM -query $QUERY -out $BOUTPUT -db $DB -evalue $EVAL -num_threads $CPU -max_target_seqs $MAXSEQS -outfmt '6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore saccver slen qlen nident positive staxids qcovs'

