#!/usr/local/bin/perl
use warnings;
use strict;
use Getopt::Std;
use File::Spec;
use Cwd;
use Bio::DB::Taxonomy;
use Bio::Tree::Tree;

###############################################
#
# calculate_alien_index.pl 
# 
# Jen Wisecaver
# 20141124 
#
# Run: perl calculate_alien_index.pl [OPTIONS]
#
################################################

# usage
my $dir = getcwd;
my %opts; getopt('bdgk', \%opts );

my $usage = "\n\tUsage Error:
	Run: perl parse_blast.pl -b [blastfile] -g [groupid] -k [skipid] [options]

	OPTIONS =
	Files:
	-b: <FULL_FILENAME> BLASTFILE: path to BLAST output in custom tab delimited format (see README)
	-d: <FILENAME> DB: BLAST database

	General:
	-g: <INTEGER> GROUPID: ncbi taxonomy id of clade that vertically inherited query sequences should group within
	-k: <INTEGER> SKIPID: ncbi taxonomy id of clade which encompases the query sequences
		
	\n\n";

# Files
my ($BLASTFILE, $BLASTPATH);
if ($opts{b}) { 
	$BLASTPATH = "$opts{b}";
	if (-e "$opts{b}") {
		my @filepath = File::Spec->splitpath($opts{b}); 
		$BLASTFILE = $filepath[2];
	}
	else { die "\n\tError: Input file <$BLASTPATH> not found.\n\tInput <b>$usage"; }
}
else { die "\n\tInput <b>$usage"; }

my $DB;
if ($opts{d}) { 
	$DB = "$opts{d}";
}
else { die "\n\tInput <d>$usage"; }

# General
my $GROUPID; 
if ($opts{g}) { if ($opts{g} =~ m/^\d+$/) { 
	if (int($opts{g}) eq $opts{g}) { $GROUPID = "$opts{g}"; }} else { die "\n\tInput <g>\n$usage"; } }
else { die "\n\tInput <g>\n$usage"; }

my $SKIPID; 
if ($opts{k}) { if ($opts{k} =~ m/^\d+$/) { 
	if (int($opts{k}) eq $opts{k}) { $SKIPID = "$opts{k}"; }} else { die "\n\tInput <k>\n$usage"; } }
else { die "\n\tInput <k>\n$usage"; }



# blast outfmt: qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore saccn slen qlen nident positive staxids qcovs"
open (IFIL, '<', $BLASTPATH) or die "Couldn't open file $BLASTPATH: $!\n";

my $logfile = $BLASTFILE . '.log';
$logfile =~ s/blast/alien/;
open (LOG, '>', $logfile) or die "Couldn't open file $logfile: $!\n";

my $outfile = $BLASTFILE . '.txt';
$outfile =~ s/blast/alien/;
open (OFIL, '>', $outfile) or die "Couldn't open file $outfile: $!\n";

# initialize global variables 
my $ncbi_db = Bio::DB::Taxonomy->new(-source => 'entrez');
my $lineages = "../scripts/lineages.txt";

my %lin;
open (LFIL, '<', $lineages) or die "Couldn't open file $lineages: $!\n";
while ( my $line = <LFIL> ) {
	chomp $line;
	my @col = split(/\t/, $line);
	$lin{$col[1]} = $col[0];
}

close LFIL;


# Parse BLAST
print LOG "parsing blasthits from $BLASTFILE ..........\n";
my %evalues;
my %lineages;
my %tophits;
my %descriptions;
my %skipped;
while ( my $line = <IFIL> ) {
	chomp $line;
	my @col = split(/\t/, $line);
	
	my $qseqid = $col[0];
	
	unless (exists $evalues{$qseqid}{'group'} && exists $evalues{$qseqid}{'other'} && exists $lineages{$qseqid}[3]){
		my $sseqid = $col[1];
		my $evalue = $col[10];
		my $bitscore = $col[11];
		
		# parse subject accession number
		my $saccn;
		if ($sseqid =~ /^[^\|]+\|([^\|]+)/){
			$saccn = $1;
		}else{
			$saccn = $sseqid;
		}

		#get subject description and store top 4 descriptions in hash
		my $sdesc = `blastdbcmd -db $DB -entry $saccn -outfmt "%t" | head -n 1`;
		chomp $sdesc;
		unless (exists $descriptions{$qseqid}[3] ){
			push @{$descriptions{$qseqid}}, $sdesc;
		}
		
		#get subject scientific name
		my $sname = `blastdbcmd -db $DB -entry $saccn -outfmt "%S" | head -n 1`;
		chomp $sname;
		$sname =~ s/\W/_/g;	
	
		#get subject taxonomy id
		my @staxids = split(/;/, $col[17]);
		my $staxid = $staxids[-1];
		
		unless ($staxid =~ /^\d+$/){
			print LOG "WARNING: $qseqid\t$sseqid\t$staxid\ttaxonomy ID undefined skipping...";
			next;		
		}
		
		#get subject scientific name
		my $ncbi_taxon;
		eval {
			$ncbi_taxon = $ncbi_db->get_taxon(-taxonid => $staxid);
		};
		if( $@ ) { # an error occurred
			print LOG "WARNING: $qseqid\t$sseqid\t$staxid\tunable to query ncbi website skipping...";
			next;
		}
		if (!defined $ncbi_taxon){
			print LOG "WARNING: $qseqid\t$sseqid\t$staxid\ttaxonomy ID undefined skipping...";
			next;
		}
		
		my $sci_name = $ncbi_taxon->scientific_name();
		$sci_name =~ s/\W/_/g;	
		$sci_name =~ s/__/_/g;
		print LOG "$qseqid\t$sseqid\t$staxid\t$sci_name";		
		
		#get subject taxonomy tree	
		my $tax_tree = Bio::Tree::Tree->new(-node => $ncbi_taxon);
		my @ncbi_lineage = $tax_tree->get_nodes;
		my @rev_ncbi_lineage = reverse @ncbi_lineage;
		my @tids;
		my %ncbi_lineage_hash;

		foreach my $taxon (@rev_ncbi_lineage) {
			my $taxon_id = $taxon->id();
			$ncbi_lineage_hash{$taxon_id} = 1;
		}

		#check if subject belongs to NCBI 'other sequences' 28384
		if (exists $ncbi_lineage_hash{'28384'}){next;}

		my $slin;
		#check if subject belongs to $SKIPID clade
		unless (exists $ncbi_lineage_hash{$SKIPID}){
			foreach my $taxon (@rev_ncbi_lineage) {
				my $taxon_id = $taxon->id();
				if (exists $lin{$taxon_id}){
					$slin = $lin{$taxon_id};
					last;
				}
			}

			unless (exists $lineages{$qseqid}[3] ){
				push @{$lineages{$qseqid}}, $slin;
			}
		}
		
		unless (exists $tophits{$qseqid}){
			my $tophit_lin;
			foreach my $taxon (@rev_ncbi_lineage) {
				my $taxon_id = $taxon->id();
				if (exists $lin{$taxon_id}){
					$tophit_lin = $lin{$taxon_id};
					last;
				}
			}
			@{$tophits{$qseqid}} = ($evalue, $sseqid, $sci_name, $staxid, $tophit_lin, $bitscore);
			
		}
		
		#check if subject belongs to $GROUPID clade
		if (exists $ncbi_lineage_hash{$GROUPID} && !exists $ncbi_lineage_hash{$SKIPID}){
			print LOG "\tGROUP";
			unless (exists $evalues{$qseqid}{'group'}){
				print LOG "\tstored\t$evalue";
				@{$evalues{$qseqid}{'group'}} = ($evalue, $sseqid, $sci_name, $staxid, $bitscore);
			}
		}
		
		#store as other if subject does not belong to $SKIPID or $GROUPID
		if (!exists $ncbi_lineage_hash{$GROUPID} && !exists $ncbi_lineage_hash{$SKIPID}){
			print LOG "\tOTHER";
			unless (exists $evalues{$qseqid}{'other'}){
				print LOG "\tstored\t$evalue";
				@{$evalues{$qseqid}{'other'}} = ($evalue, $sseqid, $sci_name, $staxid, $bitscore);
			}
		}
		
		print LOG "\n";
	}
}
close IFIL;


print LOG "calculating alien index for $BLASTFILE ..........\n";

foreach my $qseqid (keys %evalues){
	my @unique = uniq(@{$lineages{$qseqid}});
	my $descs = join( ',', @{$descriptions{$qseqid}} );
	
	my $tophit_evalue = 1;
	my $tophit_bitscore = 0;
	my $tophit_id = 'na';
	my $tophit_name = 'na';
	my $tophit_tax = 'na';
	my $tophit_lin = 'na';	

	my $group_evalue = 1;
	my $group_bitscore = 0;
	my $group_id = 'na';
	my $group_name = 'na';
	my $group_tax = 'na';

	my $other_evalue = 1;
	my $other_bitscore = 0;
	my $other_id = 'na';
	my $other_name = 'na';
	my $other_tax = 'na';
	
	if (exists $tophits{$qseqid}){
		$tophit_evalue = $tophits{$qseqid}[0];
		$tophit_id = $tophits{$qseqid}[1];
		$tophit_name = $tophits{$qseqid}[2];
		$tophit_tax = $tophits{$qseqid}[3];
		$tophit_lin = $tophits{$qseqid}[4];
		$tophit_bitscore = $tophits{$qseqid}[5];
	}

	if (exists $evalues{$qseqid}{'other'}){
		$other_evalue = $evalues{$qseqid}{'other'}[0];
		if($other_evalue == 0){$other_evalue = 9e-181};
		$other_id = $evalues{$qseqid}{'other'}[1];
		$other_name = $evalues{$qseqid}{'other'}[2];
		$other_tax = $evalues{$qseqid}{'other'}[3];
		$other_bitscore = $evalues{$qseqid}{'other'}[4];	}

	if (exists $evalues{$qseqid}{'group'}){
		$group_evalue = $evalues{$qseqid}{'group'}[0];
		if($group_evalue == 0){$group_evalue = 9e-181};
		$group_id = $evalues{$qseqid}{'group'}[1];
		$group_name = $evalues{$qseqid}{'group'}[2];
		$group_tax = $evalues{$qseqid}{'group'}[3];
		$group_bitscore = $evalues{$qseqid}{'group'}[4];
	}
	
	my $ai = (log($group_evalue) + 9e-181) - (log($other_evalue) + 9e-181);
	
	if ($group_evalue == 1 && $other_evalue == 1){
		print OFIL "$qseqid\t$tophit_id\t$tophit_name\t$tophit_tax\t$tophit_lin\t$tophit_evalue\t$tophit_bitscore\t\t\t\t\t\t\t\t\t\t\tno significant hits\t\t$descs\n";
	}else{
		print OFIL "$qseqid\t$tophit_id\t$tophit_name\t$tophit_tax\t$tophit_lin\t$tophit_evalue\t$tophit_bitscore\t$group_id\t$group_name\t$group_tax\t$group_evalue\t$group_bitscore\t$other_id\t$other_name\t$other_tax\t$other_evalue\t$other_bitscore\t$ai\t@unique\t$descs\n";
	}
}

close LOG;
close OFIL;

sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}
