#!/bin/bash
### Request email when job begins and ends
#PBS -m bea

echo $PBS_JOBID
echo $BLASTDB

cd $CWD

echo -e "query sequence id\ttop hit sequence id\ttop hit species name\ttop hit taxonomy id\ttop hit lineage\ttop hit evalue\ttop hit bitscore\tgroup hit sequence id\tgroup hit species name\tgroup hit taxonomy id\tgroup hit evalue\tgroup hit bitscore\tother hit sequence id\tother hit species name\tother hit taxonomy id\tother hit evalue\tother hit bitscore\talien index\tlineages of four top not skipped hits\tdescriptions of four top hits" > combinedresults_alienindex.txt
cat $CWD/alien-out/*txt >> combinedresults_alienindex.txt
